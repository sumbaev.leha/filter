const express = require('express');
require('dotenv').config();
const bodyParser = require('body-parser');
const router = express.Router();
const app = express();
const port = process.env.PORT || 3065;

app.use(bodyParser.text({type:"*/*"}));

app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
});

app.use('/api', router);

router.get('/catalog', (req, res) => {
  let cards = [
    {
      id: '1',
      title: "text 1",
      type: "type 1",
      date: 2018,
    },
    {
      id: '2',
      title: "text 2",
      type: "type 2",
      date: 2019,
    },
    {
      id: '3',
      title: "text 3",
      type: "type 3",
      date: 2020,
    },
    {
      id: '4',
      title: "text 4",
      type: "type 4",
      date: 2021,
    },
    {
      id: '5',
      title: "text 5",
      type: "type 1",
      date: 2022,
    },
    {
      id: '6',
      title: "text 6",
      type: "type 2",
      date: 2018,
    },
    {
      id: '7',
      title: "text 7",
      type: "type 3",
      date: 2019,
    },
    {
      id: '8',
      title: "text 8",
      type: "type 4",
      date: 2020,
    },
    {
      id: '9',
      title: "text 9",
      type: "type 1",
      date: 2021,
    },
    {
      id: '10',
      title: "text 0",
      type: "type 2",
      date: 2022,
    },
    {
      id: '11',
      title: "text 11",
      type: "type 3",
      date: 2018,
    },
    {
      id: '12',
      title: "text 12",
      type: "type 4",
      date: 2019,
    },
    {
      id: '13',
      title: "text 1",
      type: "type 1",
      date: 2018,
    },
    {
      id: '14',
      title: "text 2",
      type: "type 2",
      date: 2019,
    },
    {
      id: '15',
      title: "text 3",
      type: "type 3",
      date: 2020,
    },
    {
      id: '16',
      title: "text 4",
      type: "type 4",
      date: 2021,
    },
    {
      id: '17',
      title: "text 5",
      type: "type 1",
      date: 2022,
    },
    {
      id: '18',
      title: "text 6",
      type: "type 2",
      date: 2018,
    },
    {
      id: '19',
      title: "text 7",
      type: "type 3",
      date: 2019,
    },
    {
      id: '20',
      title: "text 8",
      type: "type 4",
      date: 2020,
    },
    {
      id: '21',
      title: "text 9",
      type: "type 1",
      date: 2021,
    },
    {
      id: '22',
      title: "text 0",
      type: "type 2",
      date: 2022,
    },
    {
      id: '23',
      title: "text 11",
      type: "type 3",
      date: 2018,
    },
    {
      id: '24',
      title: "text 12",
      type: "type 4",
      date: 2019,
    },
    {
      id: '23',
      title: "text 1",
      type: "type 1",
      date: 2018,
    },
    {
      id: '24',
      title: "text 2",
      type: "type 2",
      date: 2019,
    },
    {
      id: '25',
      title: "text 3",
      type: "type 3",
      date: 2020,
    },
    {
      id: '26',
      title: "text 4",
      type: "type 4",
      date: 2021,
    },
    {
      id: '27',
      title: "text 5",
      type: "type 1",
      date: 2022,
    },
    {
      id: '28',
      title: "text 6",
      type: "type 2",
      date: 2018,
    },
    {
      id: '29',
      title: "text 7",
      type: "type 3",
      date: 2019,
    },
    {
      id: '30',
      title: "text 8",
      type: "type 4",
      date: 2020,
    },
    {
      id: '31',
      title: "text 9",
      type: "type 1",
      date: 2021,
    },
    {
      id: '32',
      title: "text 0",
      type: "type 2",
      date: 2022,
    },
    {
      id: '33',
      title: "text 11",
      type: "type 3",
      date: 2018,
    },
    {
      id: '34',
      title: "text 12",
      type: "type 4",
      date: 2019,
    },
    {
      id: '35',
      title: "text 1",
      type: "type 1",
      date: 2018,
    },
    {
      id: '36',
      title: "text 2",
      type: "type 2",
      date: 2019,
    },
    {
      id: '37',
      title: "text 3",
      type: "type 3",
      date: 2020,
    },
    {
      id: '16',
      title: "text 4",
      type: "type 4",
      date: 2021,
    },
    {
      id: '38',
      title: "text 5",
      type: "type 1",
      date: 2022,
    },
    {
      id: '39',
      title: "text 6",
      type: "type 2",
      date: 2018,
    },
    {
      id: '40',
      title: "text 7",
      type: "type 3",
      date: 2019,
    },
    {
      id: '41',
      title: "text 8",
      type: "type 4",
      date: 2020,
    },
    {
      id: '42',
      title: "text 9",
      type: "type 1",
      date: 2021,
    },
    {
      id: '43',
      title: "text 0",
      type: "type 2",
      date: 2022,
    },
    {
      id: '44',
      title: "text 11",
      type: "type 3",
      date: 2018,
    },
    {
      id: '45',
      title: "text 12",
      type: "type 4",
      date: 2019,
    },
  ]
  if (req.query.type) {
    cards = cards.filter((item) => item.type === req.query.type)
  }

  if (req.query.date) {
    cards = cards.filter((item) => item.date === Number(req.query.date))
  }

  res.send({
    cards: cards.slice(((req.query.page - 1) * 6), (req.query.page * 6)),
    total: cards.length,
  });
});


