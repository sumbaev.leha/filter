import { CatalogPage } from '@/widgets/catalog-page';

[].slice
  .call(document.querySelectorAll('.catalog-page'))
  .forEach((parent) => {
    CatalogPage(parent);
  });
