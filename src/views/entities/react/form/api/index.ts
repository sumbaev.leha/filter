import { api } from '@/shared/api';

export interface TFormCallbackData {
  name: string
  phone: string
  [x: string]: any
}

export async function handleSubmitData(action: string, data: TFormCallbackData): Promise<any> {
  try {
    return await api.post(action, data);
  } catch (error) {
    console.error(error)
  }
}
