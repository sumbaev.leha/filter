import { Controller, useForm } from 'react-hook-form';
import './custom-form.scss';
import cn from 'classnames';
import { ReactPolitics } from '@/shared/react/politics';
import { ReactButton } from '@/shared/react/button';
import { WrapperField } from '@/shared/react/wrapper-field/input';
import { type TReactInput } from '@/shared/react/input/input';
import { clearWasteData } from '@/shared/helpers/clear-waste-data';
import { getUTMFromUrlWithCurrentPage } from '@/shared/helpers/get-UTM-from-url';
import { openPopup } from '@/shared/lib/popups';

export interface TFormFields {
  Component: TReactInput
  props: {
    label: string
    name: string
    placeholder?: string
    type?: string
  }
  isRequired?: boolean
  validation?: Function
}

export interface TForm {
  onSubmit: Function
  isResetForm?: boolean
  titleBtn?: string
  successPopupTitle?: string
  successPopupDescription?: string
  onEndingSubmit?: () => void
  isAxios?: boolean
}

export type TData = Record<string, string | number>;

export const fabricCustomForm = (props: {
  fields: TFormFields[]
}): {
  Form: FCClass<TForm>
} => {
  const { fields: formFields } = props;

  const Form: FCClass<TForm> = ({
    className,
    onSubmit,
    isResetForm = true,
    titleBtn = 'Отправить',
    onEndingSubmit,
    isAxios = false,
  }) => {
    const {
      handleSubmit,
      control,
      reset,
      formState: { errors },
    } = useForm();

    const errorValid = (name: string, errorText: string = 'Ошибка ввода'): string => errors[name] ? errorText : '';

    const handleSubmitForm = (data: TData): void => {
      if (typeof onSubmit === 'function') {
        const sendData = {
          ...clearWasteData(data),
          ...getUTMFromUrlWithCurrentPage(),
        }

        onSubmit(sendData).then((res: Record<string, { isSuccess: boolean }>) => {
          const isSuccess = isAxios ? res.data?.isSuccess : res.isSuccess;

          if (onEndingSubmit) {
            onEndingSubmit()
          }

          if (isSuccess) {
            openPopup('message', {
              isCloseAll: true,
              title: 'спасибо за заявку',
              description: 'Ваша заявка отправлена, наш менеджер перезвонит Вам в ближайшее время',
            });
            if (isResetForm) {
              reset();
            }
          } else {
            openPopup('message', {
              title: 'Ошибка отправки',
              description: 'Во время отправки заявки возникла непредвиденная ошибка. Пожалуйста, попробуйте снова',
            });
          }
        }).catch((error: any) => { console.error(error); });
      }
    }

    return (
      <form
        className={cn('custom-form', className)}
        method={'POST'}
        onSubmit={handleSubmit(handleSubmitForm) as () => void}
      >
        <div className='custom-form__wrapper'>
          <fieldset className='custom-form__fieldset'>
            {formFields.map((field) => {
              const { Component, props, isRequired, validation } = field;

              return (
                <Controller
                  key={props.name}
                  control={control}
                  name={props.name}
                  defaultValue=''
                  rules={isRequired
                    ? {
                        required: true,
                        validate: typeof validation === 'function'
                          ? {
                              validation: (input) => validation(input),
                            }
                          : undefined,
                      }
                    : undefined}
                  render={({ field: { value, name, onChange } }) => {
                    return (
                      <WrapperField
                        label={props.label}
                        className='custom-form__field'
                        error={errorValid(name)}
                      >
                        <Component
                          name={name}
                          value={value}
                          onChange={onChange}
                          placeholder={props.placeholder}
                        />
                      </WrapperField>
                    );
                  }}
                />
              )
            })}
          </fieldset>
          <ReactButton
            className={cn('custom-form__submit-button')}
            type={'submit'}
            title={titleBtn}
            disabled={Boolean(Object.keys(errors).length)}
          />
        </div>
        <ReactPolitics
          className='custom-form__politics'
        />
      </form>
    );
  }

  return {
    Form,
  }
}
