# Фабрика компонента формы:

В фабрику через массив передаются филды с компонентами, которые будут в форме, их пропсы и дополнительные флаги

### Пример:
```
const fields = [
  {
    Component: ReactInput,
    props: {
      name: 'name',
      placeholder: 'НАПИШИТЕ СВОЕ ИМЯ',
      label: 'Имя',
    },
    isRequired: true,
  },
  {
    Component: ReactInputPhone,
    props: {
      name: 'phone',
      placeholder: '+7 (___) ___-__-__',
      label: 'Телефон',
    },
    isRequired: true,
    validation: (value: string) => handlePhoneValidation(value),
  },
];
```
