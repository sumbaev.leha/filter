import { type TFormCallbackData, handleSubmitData } from '../../api';
import { fabricCustomForm } from '../../lib/custom-form';
import { ReactInput } from '@/shared/react/input/input';
import { ReactInputPhone } from '@/shared/react/input/ui/phone';
import { handlePhoneValidation } from '@/shared/react/input/helpers';

const fields = [
  {
    Component: ReactInput,
    props: {
      name: 'name',
      placeholder: 'ВВЕДИТЕ ВАШЕ ИМЯ',
      label: 'Ваше имя',
    },
    isRequired: true,
  },
  {
    Component: ReactInputPhone,
    props: {
      name: 'phone',
      placeholder: '+7 (___) ___-__-__',
      label: 'Телефон',
    },
    isRequired: true,
    validation: (value: string) => handlePhoneValidation(value),
  },
];

const { Form } = fabricCustomForm({
  fields,
});

interface TFormCallback {
  additionalData?: Record<string, any>
  action: string
}

export const FormCallback: FCClass<TFormCallback> = ({
  className,
  additionalData,
  action,
}) => {
  const handleFormSubmit = async(incData: TFormCallbackData): Promise<any> => {
    const data: TFormCallbackData = {
      ...additionalData,
      ...incData,
    };

    return await handleSubmitData(action, data);
  };

  return (
    <Form
      className={className}
      onSubmit={handleFormSubmit}
      titleBtn='отправить заявку'
    />
  );
};
