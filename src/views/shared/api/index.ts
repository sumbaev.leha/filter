export const api = {
  get: async(endpoint: string): Promise<any> => {
    const controller = new AbortController();
    const signal = controller.signal;
    const timer = setTimeout(() => {
      controller.abort();
    }, 2000);
    return await fetch(`${endpoint}`, {
      signal,
      cache: 'no-store',
    }).then(async(res) => {
      clearTimeout(timer);
      return await res.json();
    });
  },
  post: async(endpoint: string, data: Record<string, any>, hasFile: boolean = false): Promise<any> => {
    const controller = new AbortController();
    const additionalProperties: { signal?: AbortSignal } = {};
    let timer: NodeJS.Timeout;
    if (!hasFile) {
      additionalProperties.signal = controller.signal;
      timer = setTimeout(() => {
        controller.abort();
      }, 2000);
    }
    const formData = new FormData();
    Object.keys(data).forEach((key) => {
      formData.append(key, data[key]);
    });
    const requestOptions: RequestInit | any = {
      ...additionalProperties,
      cache: 'no-store',
      method: 'post',
      body: hasFile ? formData : JSON.stringify(data),
    };
    return await fetch(`${endpoint}`, requestOptions).then(async(res) => {
      clearTimeout(timer);
      return await res.json();
    });
  },
}
