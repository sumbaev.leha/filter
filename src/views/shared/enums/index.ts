import { IconsArray } from './icons';

export { IconsArray };

export type TIconId = `${IconsArray}`;

export enum MediaSizes {
  DESKTOP = 1280,
  TABLET = 768,
}

export enum POPUP_TYPES {
  standart = 'standart',
  default = '',
}
