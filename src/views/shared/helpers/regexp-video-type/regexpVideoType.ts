export const regexpVideoType = {
  youtube: /^(?:https?:\/\/)?(?:m\.|www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/,
  rutube: /^(?:https?:\/\/)?rutube\.ru\/(?:video|play\/embed)\/([a-zA-Z0-9]+)\/?(?:\?.*)?$/,
};
