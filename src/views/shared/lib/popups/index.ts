import { emitEvent } from '@/shared/helpers/emitEvent';
import { getDataPopupUrl } from './api';

/**
 * Функция открывает попап заданного варианта(варианты находятся в widgets/popups), с пропсами, переданные в объект data
 */
export const openPopup = (variant: string, data?: object): void => {
  emitEvent('modalOpen', {
    variant,
    data,
  });
}

/**
 * Отправляет запрос и открывает нужный попап по полученным данным
 */
export const openPopupThroughApi = (typePopup: string, action: string, popupType?: string, data?: object): void => {
  if (!action && !typePopup) return;

  getDataPopupUrl(action).then(res => {
    if (res) {
      openPopup(typePopup, {
        ...res,
        ...data,
        popupType,
      });
    }
  }).catch(err => { console.error(err); })
};
