import cn from 'classnames';
import './react-button-arrow.scss';

enum ReactButtonArrowVariant {
  DEFAULT = 'default',
}

export interface IReactButtonArrow {
  variant?: `${ReactButtonArrowVariant}`
}

const parentClass = 'react-button-arrow' as const;
const classes = {
  parent: parentClass,
  white: `${parentClass}--white`,
} as const;

export const ReactButtonArrow: FCClass<IReactButtonArrow> = ({
  className,
  variant = 'default',
}) => {
  return (
    <button
      className={cn(classes.parent, className)}
      type='button'
    ></button>
  );
}
