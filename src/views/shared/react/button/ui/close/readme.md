# Комопнент кнопки крестика-закрытия на реакте:
![screen](./screen.jpg?raw=true "Скриншот компонента кнопки крестика-закрытия на реакте")

Пропсы:
- className: string, Дополнительные классы

Модификатор стилей:
- `react-button-close--light-theme` - задает светлую тему

![screen](./screen-light-theme.jpg?raw=true "Скриншот со светлой темой")
