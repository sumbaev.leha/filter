import { type TIconId } from '@/shared/enums';
import { Icon } from '@/shared/react/icon';
import cn from 'classnames';
import './react-button.scss';

export interface TReactButton {
  title: string
  url?: string
  disabled?: boolean
  onClick?: () => void
  icon?: TIconId
  isAttention?: boolean
  type?: 'button' | 'submit' | 'reset'
}

export const ReactButton: FCClass<TReactButton> = ({
  className,
  title,
  url,
  disabled,
  onClick,
  icon,
  isAttention,
  type = 'button',
  ...props
}) => {
  const Tag = url ? 'a' : 'button';
  const isExternal = url ? 'url' : false;

  return (
    <Tag
      className={cn('react-button', className, { 'react-button--disabled': disabled })}
      href={url}
      onClick={() => {
        if (onClick) {
          onClick();
        }
      }}
      target={isExternal ? '_blank' : undefined}
      rel={isExternal ? 'noreferrer noopener' : undefined}
      disabled={disabled}
      type={url ? undefined : type}
      {...props}
    >
      {title && (
        <span className="react-button__content h5">
          {title}
        </span>
      )}
      {icon && (
        <div className={cn('react-button__wrapper-icon', { 'react-button__wrapper-icon--attention': isAttention })}>
          <Icon
            className={'react-button__icon'}
            icon={icon}
          />
        </div>
      )}
    </Tag>
  );
}
