# Компонент checkbox

![screen](./screen.jpg?raw=true "checkbox")

#### Пропсы:
- className: string, Дополнительные классы, модификаторы;
- title: string, Текст чекбокса;
- name: string, Имя чекбокса;
- value: string, Значение чекбокса;
- isDisabled: boolean, Флаг состояния disabled для чекбоксы;
- selected: boolean, Флаг чекнутого чекбокса;
- onChange: function, Колбак, вызывающий при изменение чекбокса;
- isLong: boolean, Флаг модификатора, убирает ограничение по ширине;
