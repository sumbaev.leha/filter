import cn from 'classnames';
import { useEffect, useState } from 'react';
import './checkbox.scss';

export interface IReactCheckbox {
  title?: string
  name?: string
  value: string
  isDisabled?: boolean
  selected?: boolean
  onChange?: (event: React.ChangeEvent<HTMLInputElement>, selected: boolean) => void
  isLong?: boolean
}

const parentClass = 'react-checkbox' as const;
const classes = {
  parent: parentClass,
  label: `${parentClass}__label`,
  input: `${parentClass}__input`,
  wrapper: `${parentClass}__wrapper`,
} as const;

export const ReactCheckbox: FCClass<IReactCheckbox> = ({
  className,
  title,
  name,
  value,
  isDisabled,
  selected,
  onChange,
  isLong,
}) => {
  const [isSelected, setIsSelected] = useState(selected ?? false);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    setIsSelected(!isSelected);

    if (typeof onChange === 'function') {
      onChange(event, !isSelected);
    }
  }

  useEffect(() => {
    setIsSelected(selected ?? false);
  }, [selected]);

  return (
    <div className={cn(classes.parent, className, { [`${classes.parent}--long`]: isLong })}>
      <label className={classes.label}>
        <input
          className={classes.input}
          type='checkbox'
          name={name}
          value={value}
          disabled={isDisabled}
          checked={isSelected}
          onChange={(event) => { handleChange(event); }}
        />
        <div className={cn(classes.wrapper, 'h5')}>
          {title}
        </div>
      </label>
    </div>
  );
}
