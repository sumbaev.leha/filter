import { type InputHTMLAttributes } from 'react';
import cn from 'classnames';
import './react-input.scss';

export type TReactInput = FCClass<InputHTMLAttributes<HTMLInputElement>>;

export const ReactInput: TReactInput = ({
  className,
  value,
  onChange,
  placeholder,
  disabled,
  ...props
}) => {
  return (
    <input
      className={cn('react-input h5', className)}
      value={value}
      onChange={onChange}
      placeholder={placeholder}
      disabled={disabled}
      name={props.name}
    />
  );
};
