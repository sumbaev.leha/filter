import { type TReactInput } from '../../input';
import InputMask from 'react-input-mask';
import cn from 'classnames';
import '../../react-input.scss';

export const ReactInputPhone: TReactInput = ({
  className,
  ...props
}) => {
  return (
    <InputMask
      type={'tel'}
      mask="+7 (999) 999-99-99"
      className={cn('react-input h5', className)}
      onChange={props.onChange}
      placeholder={props.placeholder}
      name={props.name}
      value={props.value}
    />
  )
}
