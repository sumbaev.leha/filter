import cn from 'classnames';
import './modal.scss';
import { ScrollCustom, ScrollLock } from '@/shared/lib/scroll';
import { useEffect, useRef } from 'react';
import { ReactButtonClose } from '@/shared/react/button';
import { useOutsideClick } from '@/shared/hook/useOutsideClick';
import useMatchMedia from '@/shared/hook/useMatchMedia';

const parentClass = 'modal' as const;
const classes = {
  parent: parentClass,
  wrapper: `${parentClass}__wrapper`,
  buttonClose: `${parentClass}__button-close`,
  title: `${parentClass}__title`,
  scrollWrapper: `${parentClass}__scroll-wrapper`,
  scrollMargins: `${parentClass}__scroll-margins`,
} as const;

export interface IModal {
  title?: string
  show: boolean
  closePopup: () => void
}

const bodyScrollLock = new ScrollLock();

export const ReactModal: FCClass<IModal> = ({
  className,
  title = 'Еще фильтры',
  show,
  closePopup,
  children,
}) => {
  const { isDesktop } = useMatchMedia();

  const handleCloseUp = (): void => {
    if (isDesktop) {
      closePopup();
    }
  }

  const scrollWrapperRef = useRef<HTMLDivElement>(null);
  const wrapperRef = useRef<HTMLDivElement>(null);
  useOutsideClick(wrapperRef, handleCloseUp);

  useEffect(() => {
    const closeOnEscape = (event: KeyboardEvent): void => {
      if (event.key === 'Escape') {
        closePopup();
      }
    }

    window.addEventListener('keydown', closeOnEscape);

    return () => {
      window.removeEventListener('keydown', closeOnEscape);
    }
  }, [])

  useEffect(() => {
    if (show) {
      bodyScrollLock.lock();
    } else {
      bodyScrollLock.enable();
    }
  }, [show]);

  useEffect(() => {
    let instance: ScrollCustom;
    if (scrollWrapperRef.current && !isDesktop) {
      instance = new ScrollCustom(scrollWrapperRef.current);
    }

    return () => {
      if (instance) {
        instance.destroy();
      }
    }
  }, [scrollWrapperRef.current, isDesktop]);

  return (
    <div className={cn(classes.parent, className, { [`${classes.parent}--show`]: show })}>
      <div ref={wrapperRef} className={classes.wrapper}>
        <ReactButtonClose
          className={classes.buttonClose}
          onClick={() => { closePopup(); }}
        />
        <div className={cn(classes.title, 'h4')}>
          {title}
        </div>
        <div className={classes.scrollWrapper}>
          <div ref={scrollWrapperRef} className={classes.scrollMargins} data-scroll-lock-scrollable>
            {children}
          </div>
        </div>
      </div>
    </div>
  );
};
