# Компонент modal

![screen](./screen.jpg?raw=true "modal")

#### Пропсы:
- className: string, дополнительный класс;
- title: string, заголовок модалки;
- show: booelan, открыта или закрыта модалка;
- closePopup: Function, функция, которая устанавливает show в false;
