import cn from 'classnames';
import './react-politics.scss';

export interface TReactPolitics {
  text?: string
  linkText?: string
  linkUrl?: string
}

export const ReactPolitics: FCClass<TReactPolitics> = ({
  className,
  text = 'Нажимая кнопку «Отправить заявку», я соглашаюсь ',
  linkText = 'на обработку персональных данных',
  linkUrl = '/personal-data/',
}) => {
  return (
    <div className={cn('react-politics', className)}>
      <span className="react-politics__text h7">{text}</span>
      <a
        className='react-politics__link h7'
        href={linkUrl}
        rel={'noreferrer noopener'}
      >
        {linkText}
      </a>
    </div>
  );
};
