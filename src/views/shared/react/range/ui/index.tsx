import cn from 'classnames';
import './react-range.scss';
import { Range, getTrackBackground } from 'react-range';
import { useEffect, useState } from 'react';
import { ReactRangeInput, ReactRangeInputVariant } from './input';
import useMatchMedia from '@/shared/hook/useMatchMedia';

const parentClass = 'react-range' as const;
const classes = {
  parent: parentClass,
  inputs: `${parentClass}__inputs`,
  input: `${parentClass}__input`,
  wrapperInput: `${parentClass}__wrapper-input`,
  inputText: `${parentClass}__input-text`,
  measurement: `${parentClass}__measurement`,
  trackOuter: `${parentClass}__track-outer`,
  trackInner: `${parentClass}__track-inner`,
  thumb: `${parentClass}__thumb`,
} as const;

export interface IReactRange {
  step?: number
  min?: number
  max?: number
  values?: number[]
  measurement?: string
  onChange?: (values: number[]) => void
  onFinalChange?: (values: number[]) => void
  onNewValues?: (values: number[]) => void
}

export const ReactRange: FCClass<IReactRange> = ({
  className,
  step = 1,
  min: initialMin = 0,
  max: initialMax = 100,
  measurement = '',
  values: initialValues = [10],
  onChange,
  onFinalChange,
  onNewValues,
}) => {
  const [values, setValues] = useState(initialValues);
  const [max, setMax] = useState(initialMax);
  const [min, setMin] = useState(initialMin);
  const [isHover, setIsHover] = useState(false);

  const { isDesktop } = useMatchMedia();

  useEffect(() => {
    setMax(initialMax);
  }, [initialMax]);

  useEffect(() => {
    setMin(initialMin);
  }, [initialMin]);

  useEffect(() => {
    setValues([...initialValues]);

    if (typeof onNewValues === 'function') {
      onNewValues([...initialValues]);
    }
  }, [...initialValues]);

  const handleSingleValue = (result: string): void => {
    const numberResult = Number(result);

    const newArray = [...values];
    newArray[0] = numberResult;

    setValues(newArray);

    if (typeof onChange === 'function') {
      onChange(newArray);
    }

    if (typeof onFinalChange === 'function') {
      onFinalChange(newArray);
    }
  }

  const handleMultiValue = (result: string, index: number): void => {
    const numberResult = Number(result);

    let newArray = [...values];
    newArray[index] = numberResult;
    if (newArray[0] > newArray[1]) {
      newArray = [newArray[index], newArray[index]];
    }

    setValues(newArray);

    if (typeof onChange === 'function') {
      onChange(newArray);
    }

    if (typeof onFinalChange === 'function') {
      onFinalChange(newArray);
    }
  }

  return (
    <div className={cn(classes.parent, className)}
      onMouseEnter={() => { setIsHover(true); }}
      onMouseLeave={() => { setIsHover(false) }}
    >
      <div className={classes.inputs}>
        {values.length === 1 && (
          <div className={cn(classes.wrapperInput, `${classes.wrapperInput}--single`)}>
            <ReactRangeInput
              className={classes.input}
              value={values[0]}
              max={max}
              min={min}
              onKeyUp={(event: any, result: string) => { handleSingleValue(result); }}
              onBlur={(event: any, result: string) => { handleSingleValue(result); }}
            />
            <div className={cn(classes.measurement, `${classes.measurement}--last`, 'h5')}>{measurement}</div>
          </div>
        )}
        {values.length > 1 && (
          <>
            <div className={classes.wrapperInput}>
              <ReactRangeInput
                className={classes.input}
                variant={ReactRangeInputVariant.LEFT}
                value={values[0]}
                max={max}
                min={min}
                onKeyUp={(event: any, result: string) => { handleMultiValue(result, 0); }}
                onBlur={(event: any, result: string) => { handleMultiValue(result, 0); }}
              />
              <div className={cn(classes.measurement, 'h5')}>{measurement}</div>
            </div>
            <div className={cn(classes.wrapperInput, `${classes.wrapperInput}--left`)}>
              <ReactRangeInput
                className={classes.input}
                variant={ReactRangeInputVariant.RIGHT}
                value={values[1]}
                max={max}
                min={min}
                onKeyUp={(event: any, result: string) => { handleMultiValue(result, 1); }}
                onBlur={(event: any, result: string) => { handleMultiValue(result, 1); }}
              />
              <div className={cn(classes.measurement, `${classes.measurement}--last`, 'h5')}>{measurement}</div>
            </div>
          </>
        )}
      </div>
      <Range
        values={values}
        step={step}
        min={min}
        max={max}
        onChange={(values) => {
          setValues(values);

          if (typeof onChange === 'function') {
            onChange(values);
          }
        }}
        onFinalChange={(values) => {
          if (typeof onFinalChange === 'function') {
            onFinalChange(values);
          }
        }}
        renderTrack={({ props, children }) => (
          <div
            className={cn(classes.trackOuter, values.length === 1 && `${classes.trackOuter}--single`)}
            onMouseDown={props.onMouseDown}
            onTouchStart={props.onTouchStart}
            style={{
              ...props.style,
            }}
          >
            <div
              className={classes.trackInner}
              ref={props.ref}
              style={{
                background: getTrackBackground({
                  values,
                  colors: values.length <= 1 ? [`${isHover && isDesktop ? '#888888' : '#474A56'}`, '#FFFFFF00'] : ['#FFFFFF00', `${isHover && isDesktop ? '#888888' : '#474A56'}`, '#FFFFFF00'],
                  min,
                  max,
                }),
              }}
            >
              {children}
            </div>
          </div>
        )}
        renderThumb={({ props, isDragged }) => (
          <div
            className={cn(classes.thumb, { [`${classes.thumb}--grabbed`]: isDragged })}
            {...props}
            style={{
              ...props.style,
            }}
          ></div>
        )}
      />
    </div>
  );
};
