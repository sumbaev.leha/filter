import cn from 'classnames';
import './react-range-input.scss';
import { type ChangeEvent, type KeyboardEvent, type FocusEvent, useState, useEffect } from 'react';

export const transformString = (string: string): string => {
  return `${string.replace(/,/g, '.').replace(/[^.0-9]/g, '')}`;
}

export const prettify = (string: string): string => {
  const separator = ' ';
  return string.replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, '$1' + separator);
}

const parentClass = 'react-range-input' as const;
const classes = {
  parent: parentClass,
  input: `${parentClass}__input`,
  dummy: `${parentClass}__dummy`,
} as const;

export enum ReactRangeInputVariant {
  DEFAULT = '',
  LEFT = 'react-range-input--left',
  RIGHT = 'react-range-input--right',
}

export interface IReactRangeInput {
  value: number
  max: number
  min: number
  onBlur: Function
  onKeyUp: Function
  variant?: ReactRangeInputVariant
}

export const ReactRangeInput: FCClass<IReactRangeInput> = ({
  className,
  value: initialValue,
  max,
  min,
  onBlur,
  onKeyUp,
  variant = ReactRangeInputVariant.DEFAULT,
}) => {
  const [controlledValue, setControlledValue] = useState(prettify(`${initialValue}`));
  const [keyUpTimer, setKeyUpTimer] = useState<ReturnType<typeof setTimeout> | string>('');

  useEffect(() => {
    setControlledValue(prettify(`${initialValue}`));
  }, [initialValue]);

  const checkMinMax = (result: string): string => {
    const numberResult = Number(result);

    if (numberResult < min) {
      return `${min}`;
    }

    if (numberResult > max) {
      return `${max}`;
    }

    return `${numberResult}`;
  }

  const handleChange = (event: ChangeEvent<HTMLInputElement>): void => {
    /**
     * Удаляет все, кроме цифр и точек, и не позволяет ставить вторую точку
     */
    const result = event.target.value.replace(/[^.0-9]/g, '').replace(/(\.)([^.]*?)\./, '$1$2');

    setControlledValue(prettify(result));
  }

  const handleKeyUp = (event: KeyboardEvent<HTMLInputElement>): void => {
    const onChangeDuration = 2000;

    clearTimeout(keyUpTimer);
    setKeyUpTimer(setTimeout(() => {
      const result = checkMinMax(transformString((event.target as HTMLInputElement).value));
      setControlledValue(() => prettify(result));

      if (typeof onKeyUp === 'function') {
        onKeyUp(event, result);
      }

      setKeyUpTimer('');
    }, onChangeDuration));
  }

  const handleBlur = (event: FocusEvent<HTMLInputElement> | KeyboardEvent<HTMLInputElement>): void => {
    if (keyUpTimer) {
      clearTimeout(keyUpTimer);
      setKeyUpTimer('');

      const result = checkMinMax(transformString((event.target as HTMLInputElement).value));
      setControlledValue(() => prettify(result));

      if (typeof onBlur === 'function') {
        onBlur(event, result);
      }
    }
  }

  return (
    <div className={cn(classes.parent, className, [variant])}>
      <div className={cn(classes.dummy, 'h5')}>{controlledValue}</div>
      <input
        className={cn(classes.input, 'h5')}
        type='text'
        value={controlledValue}
        onChange={(event) => { handleChange(event); }}
        onBlur={(event) => { handleBlur(event); }}
        onKeyUp={(event) => {
          if (event.key === 'Enter' || event.key === 'NumpadEnter' || event.key === 'Tab') {
            handleBlur(event);
          } else {
            handleKeyUp(event);
          }
        }}
      />
    </div>
  );
};
