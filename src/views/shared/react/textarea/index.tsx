import { type TextareaHTMLAttributes, type ChangeEvent, useCallback } from 'react';
import cn from 'classnames';
import './react-textarea.scss';

export type TReactTextarea = FCClass<TextareaHTMLAttributes<HTMLTextAreaElement>>;

export const ReactTextarea: TReactTextarea = ({
  className,
  onChange,
  children,
  ...props
}) => {
  const setTextareaHeight = useCallback((event: ChangeEvent<HTMLElement>) => {
    event.target.style.height = 'auto';
    event.target.style.height = `${event.target.scrollHeight}px`;
  }, []);

  return (
    <textarea className={cn('react-textarea h5', className)}
      onChange={event => {
        setTextareaHeight(event);

        if (typeof onChange === 'function') {
          onChange(event);
        }
      }}
      rows={1}
      name={props.name}
      placeholder={props.placeholder}
      value={props.value}
      maxLength={props.maxLength}
    >
    </textarea>
  );
}
