import cn from 'classnames';
import Tippy from '@tippyjs/react';
import './wrapper-field.scss';
import { Icon } from '@/shared/react/icon';
import { IconsArray } from '@/shared/enums';

export interface IWrapperFiledProps {
  error?: string
  label?: string
  tooltip?: string
}

export const WrapperField: FCClass<IWrapperFiledProps> = ({
  className,
  error,
  label,
  tooltip,
  children,
}) => {
  return (
    <div className={cn('wrapper-field', error && 'wrapper-field--error', className)}>
      {label && (
        <label className='wrapper-field__label h7'>
          {label}
          {tooltip &&
            <Tippy
              theme='quiz'
              maxWidth={250}
              placement='bottom'
              content={
                <div className={'t2'}>
                  {tooltip}
                </div>
              }
            >
              <svg className={'wrapper-field__tippy-label'}>
                <use href={'/assets/svg/sprite.svg#tooltip'} />
              </svg>
            </Tippy>
          }
        </label>
      )}
      {children}
      {Boolean(error) &&
        <Tippy
          theme='light'
          placement='bottom'
          appendTo={(element) => element.parentElement as HTMLElement}
          content={
            <div className='wrapper-field__tippy t4'>
              {error}
            </div>
          }
          animation={'scale'}
          offset={[0, 12]}
        >
          <Icon className='wrapper-field__error-icon' icon={IconsArray.formErrorIcon} />
        </Tippy>
      }
    </div>
  );
};
