export type TPopupChild<K = object> = FCClass<{
  closePopup: () => void
} & K>;
