export interface ILink {
  title?: string
  icon?: string
}
