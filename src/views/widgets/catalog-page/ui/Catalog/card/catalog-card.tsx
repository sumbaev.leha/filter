import React from 'react';
import cn from 'classnames';
import './catalog-card';

export interface TCatalogCard {
  id?: number
  type?: string
  date?: number
  title: string
}

export const CatalogCard: FCClass<TCatalogCard> = ({
  className,
  type,
  date,
  title,
}) => {
  return (
    <div className={cn('catalog-card', className)}>
      <div className="catalog-card__top">
        <div className="catalog-card__title">no image</div>
      </div>
      <div className="catalog-card__bottom">
        {type && <div className="catalog-card__type">{type}</div>}
        <div className="catalog-card__title">{title}</div>
        {date && <div className="catalog-card__date">{date}</div>}
      </div>
    </div>
  )
}
