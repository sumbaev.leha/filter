import React, { useEffect, useState } from 'react';
import cn from 'classnames';
import { api } from '@/shared/api';
import { ReactButton } from '@/shared/react/button';
import { CatalogCard, type TCatalogCard } from '@/widgets/catalog-page/ui/Catalog/card/catalog-card';
import './catalog';

const Catalog = ({ apiCatalog }: { apiCatalog: string }) => {
  /**
   *
   * @param {array} cardsData Массив карточек
   * @param {number} total число карточек выбранного типа
   * @param {number} page Страница для запроса карточек
   * @param {object} activeType Выбраный/активный тип опций для запроса
   * @param {object} activeDate Выбраный/активный дата опций для запроса
   */

  const seatchParms = new URLSearchParams('')

  const type = ['type 1', 'type 2', 'type 3', 'type 4']
  const date = [2018, 2019, 2020, 2021, 2022]

  //
  const [cardsData, setCardsData] = useState<TCatalogCard[]>([]);
  const [total, setTotal] = useState(0);
  const [page, setPage] = useState(1);
  const [activeType, setActiveType] = useState<string | null>(null)
  const [activeDate, setActiveDate] = useState<number | null>(null)

  // собираю seatchParms для запроса на сервер
  if (activeDate) {
    seatchParms.append('date', activeDate.toString())
  }
  if (activeType) {
    seatchParms.append('type', activeType)
  }
  if (page) {
    seatchParms.append('page', page.toString())
  }

  // вызывается при нажатии на tab тип
  const handleType = (tab: string | null): void => {
    // очищаю список карточек
    setCardsData([])
    // сбрасываю страницу до 1
    setPage(1)
    // меняю активный тип на выбранный
    setActiveType(tab)
  }

  // вызывается при нажатии на tab дату
  const handleDate = (tab: number | null): void => {
    setCardsData([])
    setPage(1)
    setActiveDate(tab)
  }

  // запрос на сервер
  useEffect(() => {
    // в запрос уходит apiCatalog и собранный seatchParms
    api.get(`${apiCatalog}?${seatchParms.toString()}`)
      .then((data) => {
        // с сервера ожидаю получить список карточек и добавляю его к существуещему списку
        setCardsData([...cardsData, ...data.cards])
        // с сервера ожидаю получить количество карточек
        setTotal(data.total)
      }).catch(err => { console.error(err); });
    // делаю запрос при изменении page, activeType, activeDate
  }, [page, activeType, activeDate]);

  return (
    <div className={cn('catalog')}>
      {/*отрисовываю табы*/}
      {type.length > 0 && <div className="catalog__filter">
        {type.map((tab) => {
          return (
            <ReactButton
              key={tab}
              disabled={activeType === tab}
              title={tab}
              onClick={() => { handleType(tab) }}
            />
          )
        })}
        <ReactButton
          title={'все типы'}
          disabled={activeType === null}
          onClick={() => { handleType(null) }}
        />
      </div>}
      {/*отрисовываю карточки*/}
      {date.length > 0 && <div className="catalog__filter">
        {date.map((tab) => {
          return (
            <ReactButton
              key={tab}
              disabled={activeDate === tab}
              title={tab.toString()}
              onClick={() => { handleDate(tab) }}
            />
          )
        })}
        <ReactButton
          title={'все даты'}
          disabled={activeDate === null}
          onClick={() => { handleDate(null) }}
        />
      </div>}

      <div className="catalog__wrapper">
        {(cardsData?.length > 0)
          ? <div className="catalog__list-wrapper">
          <div className="catalog__list">
            {cardsData.map((card) => (
              <CatalogCard
                key={card.id}
                className={'catalog__card'}
                title={card.title}
                type={card.type}
                date={card.date}
              />
            ))}
          </div>
          {(total > cardsData.length) && <ReactButton
            title='загрузить ещё'
            className="catalog__button"
            onClick={() => { setPage(page + 1) }}
          />}
        </div>
          : <div>Таких карточек нет</div>
        }
      </div>
    </div>
  );
}

export default Catalog;
