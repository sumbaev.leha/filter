import React from 'react';
import { createRoot } from 'react-dom/client';
import Catalog from '@/widgets/catalog-page/ui/Catalog/catalog';

export const CatalogPage = (parent: HTMLElement): void => {
  const root = parent.querySelector('#catalog-root')
  const apiCatalog = parent.dataset.api

  if (root && apiCatalog) {
    const reactRoot = createRoot(root);
    reactRoot.render(<Catalog apiCatalog={apiCatalog}/>)
  }
}
